import { combineReducers } from 'redux';
import system from '../features/system/reducers';


export default function createReducer(injectedReducers = {}) {
  const rootReducer = combineReducers({
    systemReducer: system,
  });

  return rootReducer;
};