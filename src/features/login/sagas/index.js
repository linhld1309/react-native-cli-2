import {takeEvery} from 'redux-saga/effects';
import * as types from '../actionTypes';
import loginSaga from './loginSaga';

export const loginSagas = [takeEvery(types.LOGIN_REQUEST, loginSaga)];
