rt produce from 'immer';
import {AsyncStorage} from 'react-native';
import {persistReducer} from 'redux-persist';

import * as types from './actionTypes';

// const TAG = 'LoginReducer';

const persistConfig = {
  key: 'global',
  storage: AsyncStorage,
  whitelist: ['accessToken'],
};

const initialState = {
  isLoggedIn: false,
  id: -1,
  username: '',
  password: '',
};

const reducer = (state = initialState, action) =>
  produce(state, (newState) => {
    switch (action.type) {
      case types.LOGIN_REQUEST:
        newState.username = action.username;
        newState.password = action.password;
        break;
      case types.LOGIN_RESPONSE:
        newState.id = action.id;
        newState.isLoggedIn = true;
        break;
      case types.LOGIN_FAILED:
        newState.isLoggedIn = false;
        break;
    }
  });

export default persistReducer(persistConfig, reducer);
