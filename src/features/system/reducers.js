import produce from 'immer';
import {AsyncStorage} from 'react-native';
import {persistReducer} from 'redux-persist';

import * as types from './actionTypes';

const persistConfig = {
  key: 'system',
  storage: AsyncStorage,
  whitelist: ['infoAccount'],
};

const initialState = {
  infoAccount: {},
};

const reducer = (state = initialState, action) =>
  produce(state, (newState) => {
    switch (action.type) {
      case types.REGISTER_INFO_ACCOUNT:
        newState.infoAccount = action.payload;
        break;
      case types.RESET_INFO_ACCOUNT:
        newState.infoAccount = {};
        break;
    }
  });

export default persistReducer(persistConfig, reducer);