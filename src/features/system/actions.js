import * as types from './actionTypes';

export function actionRegisterInfoAccount(infoAccount) {
  return {
    type: types.REGISTER_INFO_ACCOUNT,
    payload: infoAccount,
  };
}

export function actionResetInfoAccount() {
  return {
    type: types.RESET_INFO_ACCOUNT,
  };
}
