1.Install Android Studio
    Android SDK
    Android SDK Platform
    Android Virtual Device
2.Install the Android SDK
    Android 9 (Pie)
    Android SDK Platform 28
    Intel x86 Atom_64 System Image or Google APIs Intel x86 Atom System Image
3.Configure the ANDROID_HOME environment variable
    export ANDROID_HOME=$HOME/Android/Sdk
    export PATH=$PATH:$ANDROID_HOME/emulator
    export PATH=$PATH:$ANDROID_HOME/tools
    export PATH=$PATH:$ANDROID_HOME/tools/bin
    export PATH=$PATH:$ANDROID_HOME/platform-tools
4.Install java openjdk 8(1.8.0_252)
5.Add file local.properties in android file
    sdk.dir = $HOME/Android/Sdk
6.Running React Native application
    npx react-native start
    npx react-native run-android